#!/usr/bin/env fish



while inotifywait -e modify _includes/*.html _layouts/*.html _includes/navbars/*.html
  echo "it works"
  purgecss --css bootstrap.css --content _site/*.html --out minCSS
  purgecss --css animate.css --content _site/*.html --out minCSS

end

while inotifywait -e modify sass/Jstyle.scss 
  echo "hello"
  set header (head -n 1 Jstyle.css)
  if test "$header" = "---"
   echo "no changes"
  else
   sed -i '1s/^/---\n/' Jstyle.css
   sed -i '1s/^/---\n/' Jstyle.css
  end
end
