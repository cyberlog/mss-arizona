---
layout: post
title: WWBC
description: WWBC (world without breast cancer) is a group of people dedicated to completely irradicating breast cancer.  
image: /images/wwbc.png
link: https://worldwithoutbreastcancer.com/
gitlab: https://gitlab.com/jaynky/worldwithoutcancer
---

<a href="https://worldwithoutbreastcancer.com/">World Without Breast Cancer(WWBC)</a> wanted to have a landing page and blog to help raise money for breast cancer research. What they got was a high performing static website that increased awareness and helped raise money.