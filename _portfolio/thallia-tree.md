---
layout: post
title: Thallia Tree
description: Portfolio and blog
image: /images/thalliatree.png
link: https://thalliatree.net/
gitlab: https://gitlab.com/jaynky/thallia-tree-4-realz
---

<a href="https://thalliatree.net/">Thallia Tree</a> is a portfolio and tech blog. When we first started the project, the code was a jumbled, unintelligable mess. Instead of fixing it, we decided to completely rebuild the site from scratch. The end result was well worth the effort as it is faster, cleaner, and, most of all, easier to develop. 