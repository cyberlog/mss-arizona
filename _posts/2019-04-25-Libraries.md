---
layout: post
title:  "Libraries used in Jtheme"
date:   April 25, 2019
author: Jay
description: An outline of 3rd party software used #for meta description
comments: true
excerpt_separator: <!--more-->
alt: open laptop
image: /images/blog/laptop1.jpg
categories: libraries
---

In the previous posts I have outlined that I used a few libraries and APIs.
Here is where I dig into the specifics.

<!--more-->

Libraries:

 + -<a href="https://getbootstrap.com/docs/4.0/getting-started/introduction/">Bootstrap 4</a>
 + -<a href="https://michalsnik.github.io/aos/">Animate On Scroll (AOS)</a>
 + -<a href="https://disqus.com">Disqus</a>
 + -<a href="https://analytics.google.com/analytics/">Analytics</a>
 + -<a href="https://jekyllrb.com/">Jekyll</a>
 + -<a href="https://daneden.github.io/animate.css/">Animate css</a>
 + -<a href="https://getbootstrap.com/docs/4.0/getting-started/javascript/">Bootstrap JS</a>
 + -<a href="https://github.com/aFarkas/lazysizes">Lazy Loading</a>
 + -<a href="https://unsplash.com/">Unsplash (for all the stock photos)</a>
 + -<a href="https://icons8.com/">Icons8 (for all the icons used)</a>